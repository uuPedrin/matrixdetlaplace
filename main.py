#pedir o número de linhas
#input pedindo os números
#fazer sempre com a primeira linha
#limitar a ordem 4

arrLi=[
    [5,7,1],
    [2,8,9],
    [2,5,4]
]

def detCalc(matrix:list) -> float:
    '''
    Calcula o determinante da matriz usando Laplace
    '''
    detLi = matrix[0] #primeira linha da matriz
    det = 0
    for e in detLi:
        arrLi=[
            [5,7,1],
            [2,8,9],
            [2,5,4]
        ]
        cof = getCofactor(e,detLi,arrLi)
        eCof = ((-1)**(1 + (detLi.index(e) + 1))) * cof
        det += e * eCof

    print(f"o determinante da matriz é: {det}")
    
def getCofactor(element:int, line:list, matrix:list)->int:
    '''
    Calcula o cofator de um elemento da matriz e retorna o determinante da matriz do cofator
    '''
    cofac = matrix.copy()
    col = line.index(element)
    cofac.pop(0)

    for i,_ in enumerate(cofac):
        cofac[i].pop(col)

    if len(cofac[0]) == 2:
        det = 0
        det = (cofac[0][0] * cofac[1][1]) - (cofac[0][1] * cofac[1][0])
        return det
        
print (arrLi)
detCalc(arrLi)
